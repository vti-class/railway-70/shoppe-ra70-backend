package com.vti.shoppera70backend.repository;

import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository // Khai báo class này là 1 Repository và là 1 bean
public interface AccountRepository extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {

    // Tổng quát: key + tên thuộc tính ở trong java (viết hoa chữ cái đầu)
    // + điều kiện so sánh (Contains, Like, Between, )

    // findBy có thể trả về 1 đối tượng hoặc 1 danh sách đối tượng
    //findBy trả về nhiều hơn 1 dữ liệu -> return Account -> lỗi
    // findFirstBy… trả về 1 đối tượng đầu tiên
    // findAllBy… trả về 1 danh sách đối tượng
    Account findByUsername(String username);

    boolean existsByUsername(String username);

    List<Account> findByUsernameAndAddressAndRole(String username, String Address,Role role);

    List<Account> findByAddressAndRole(String Address,Role role);

    List<Account> findByUsernameContains(String username);

    List<Account> findByUsernameLike(String username);

    // Có 2 cách truyền câu query
    // C1: Native Query
    // C2: (Mặc định) Truyền HQL
    //@Query(value = "from Account ac where ac.username = :username") // HQL
    @Query(value = "select * from Account ac where ac.username = :username", nativeQuery = true) // SQL
    List<Account> findByUsername2(String username);

}
