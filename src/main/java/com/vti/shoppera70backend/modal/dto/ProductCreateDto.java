package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.ProductStatus;
import com.vti.shoppera70backend.modal.entity.ProductType;
import com.vti.shoppera70backend.modal.entity.ShippingUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor // Tạo ra hàm khởi tạo ko tham số
@AllArgsConstructor // Tạo ra hàm khởi tạo full tham số
public class ProductCreateDto {
    @Length(min = 1, max = 255, message = "Độ dài của name phải từ 1 -> 255")
    @NotNull(message = "Không được để trông name")
    private String name;

    private ProductStatus productStatus;
    private ShippingUnit shippingUnit;
    private ProductType productType;

    @Range(min = 0, max = Long.MAX_VALUE)
    private long price;
    private String image;
}
