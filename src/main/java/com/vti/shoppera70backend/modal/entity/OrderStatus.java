package com.vti.shoppera70backend.modal.entity;

public enum OrderStatus {
    PENDING, DONE, CANCEL;
}
