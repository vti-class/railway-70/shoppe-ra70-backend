package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.config.exception.CustomException;
import com.vti.shoppera70backend.config.exception.ErrorResponseEnum;
import com.vti.shoppera70backend.modal.dto.AccountCreatDto;
import com.vti.shoppera70backend.modal.dto.LoginDto;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Role;
import com.vti.shoppera70backend.modal.entity.TokenActiveAccount;
import com.vti.shoppera70backend.repository.AccountRepository;
import com.vti.shoppera70backend.repository.TokenRepository;
import com.vti.shoppera70backend.service.IMailSenderService;
import com.vti.shoppera70backend.service.JWTTokenUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/auth")
@CrossOrigin(value = "*")
public class AuthController {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JWTTokenUtils jwtTokenUtils;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private IMailSenderService mailSenderService;

    @PostMapping("/login-v1")// API phải qua bước authen mới vào đây.
    public LoginDto loginBasicV1(Principal principal){
        // principal: ĐƯợc sinh ra tại bước authen
        String username = principal.getName();
        Account account = accountRepository.findByUsername(username);

        LoginDto loginDto = new LoginDto();
        BeanUtils.copyProperties(account, loginDto);
        return loginDto;
    }

    @PostMapping("/login-v2")
    public LoginDto loginBasicV2(@RequestParam String username, @RequestParam String password){
        // Để kiểm tra đouwcj login username và password ở đay -> cần mở public api này
        Account account = accountRepository.findByUsername(username);
        if (account == null){
            throw new CustomException(ErrorResponseEnum.LOGIN_FAIL_USERNAME);
        }
        if (!passwordEncoder.matches(password, account.getPassword())){
            throw new CustomException(ErrorResponseEnum.LOGIN_FAIL_PASSWORD);
        }
        LoginDto loginDto = new LoginDto();
        BeanUtils.copyProperties(account, loginDto);
        return loginDto;
    }

    @PostMapping("/login-jwt")
    public LoginDto loginJWT(@RequestParam String username, @RequestParam String password){
        // Check usernam & password: nếu đúng -> trả về token và các thông tin khác
        Account account = accountRepository.findByUsername(username);
        if (account == null){
            throw new CustomException(ErrorResponseEnum.LOGIN_FAIL_USERNAME);
        }
        if (!passwordEncoder.matches(password, account.getPassword())){
            throw new CustomException(ErrorResponseEnum.LOGIN_FAIL_PASSWORD);
        }
        if (!account.isActive()){
            throw new CustomException(ErrorResponseEnum.LOGIN_FAIL_NOT_ACTIVE);
        }
        LoginDto loginDto = new LoginDto();
        BeanUtils.copyProperties(account, loginDto);
        // Set giá trị token cho loginDto -> return
        String token = jwtTokenUtils.createAccessToken(loginDto);
        loginDto.setToken(token);
        return loginDto;
    }

    @GetMapping("/active/{token}")
    public String activeAccount(@PathVariable String token){
        TokenActiveAccount activeAccount = tokenRepository.findByToken(token);
        if (activeAccount == null){
            return "Mã kích hoạt không tồn tại";
        }
        Account account = activeAccount.getAccount();
        if (account.isActive()){
            return "Account đã được kích hoạt!";
        }
        account.setActive(true);
        accountRepository.save(account);
        return "Tài khoản đã được kích hoạt!";
    }

    @PostMapping("/register") // /api/v1/account/create
    public Account create(@RequestBody @Valid AccountCreatDto dto){
        Account account = new Account();
        BeanUtils.copyProperties(dto, account);

        // Set lại các giá trị mặc định
        account.setActive(false);
        account.setRole(Role.CUSTOMER);
        account.setPassword(passwordEncoder.encode(dto.getPassword())); // Set password đã mã hoá vào DB

        account = accountRepository.save(account); // Hứng giá trị của account khi thêm mới user( để lấy được giá trị id)

        // Tạo ra mã token để active account -> lưu trong bảng TokenActiveAccount
        String token = UUID.randomUUID().toString();
        TokenActiveAccount tokenActiveAccount = new TokenActiveAccount();
        tokenActiveAccount.setAccount(account);
        tokenActiveAccount.setToken(token);
        tokenRepository.save(tokenActiveAccount);
        // Gửi mail
        String text = "<div>Bạn vừa đăng ký tài khoản thành công, để kích hoạt tài khoản: <a href=\"http://127.0.0.1:8888/api/v1/auth/active/"+ token +"\" >Click Here</a></div>";
        mailSenderService.sendMessageWithAttachment(account.getEmail(), "Đăng ký tài khoản", text);

        return account;
    }

}
