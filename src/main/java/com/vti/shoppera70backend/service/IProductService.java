package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.dto.ProductCreateDto;
import com.vti.shoppera70backend.modal.dto.ProductUpdateDto;
import com.vti.shoppera70backend.modal.dto.SearchProductRequest;
import com.vti.shoppera70backend.modal.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IProductService {
    Page<Product> search(SearchProductRequest request);

    List<Product> getAll();

    Product getById(int id);

    Product create(ProductCreateDto dto);

    Product update(ProductUpdateDto dto);

    void delete(int id);
}
