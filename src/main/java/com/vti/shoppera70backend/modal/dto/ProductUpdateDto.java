package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.ProductStatus;
import com.vti.shoppera70backend.modal.entity.ProductType;
import com.vti.shoppera70backend.modal.entity.ShippingUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor // Tạo ra hàm khởi tạo ko tham số
@AllArgsConstructor // Tạo ra hàm khởi tạo full tham số
public class ProductUpdateDto {
    private int id;
    private String name;
    private ProductStatus productStatus;
    private ShippingUnit shippingUnit;
    private ProductType productType;
    private long price;
    private String image;
}
