package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.ProductStatus;
import com.vti.shoppera70backend.modal.entity.ProductType;
import com.vti.shoppera70backend.modal.entity.ShippingUnit;
import lombok.Data;

import java.util.List;

@Data
public class SearchProductRequest extends BaseRequest{
    private String productName;
    private List<ProductType> productTypes;
    private List<ShippingUnit> shippingUnits;
    private List<ProductStatus> productStatuses;
    private long minPrice;
    private long maxPrice;
}
