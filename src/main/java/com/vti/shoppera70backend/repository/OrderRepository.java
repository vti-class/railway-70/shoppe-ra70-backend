package com.vti.shoppera70backend.repository;

import com.vti.shoppera70backend.modal.entity.Order;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findAllByOrderStatusAndOrderBy_Id(OrderStatus orderStatus, int accountId);

    List<Order> findAllByOrderBy_Id(int accountId);

    List<Order> findAllByCreateAtBeforeAndOrderStatus(Date createAt, OrderStatus orderStatus);
}
