package com.vti.shoppera70backend.modal.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountUpdateDto {
    private int id;
    private String username; // username
    private String phoneNumber;
    private String information;
    private String fullName;
    private String email;
    private Date dateOfBirth;
    private String address;

}
