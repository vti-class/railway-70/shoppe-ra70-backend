package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.modal.dto.AccountCreatDto;
import com.vti.shoppera70backend.modal.dto.AccountUpdateDto;
import com.vti.shoppera70backend.modal.dto.DemoBean;
import com.vti.shoppera70backend.modal.dto.SearchAccountRequest;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.service.AccountService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import javax.validation.Valid;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

@RestController // return -> json (mô hình REST API)
//@Controller // return -> html (mô hình mvc)
@RequestMapping(value = "/api/v1/account")
@Validated
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
    @Qualifier(value = "demoBean1")
    private DemoBean demoBean2;

    @GetMapping("/get-all")// api: localhost:8080/api/v1/account
    public List<Account> getAll(){
        return accountService.getAll();
    }

    @PostMapping("/search")
    public Page<Account> search(@RequestBody SearchAccountRequest request){
        return accountService.search(request);
    }

    @GetMapping("/{id}")
    public Account getById(@PathVariable int id){
        return accountService.getById(id);
    }

    @GetMapping("/find-by-username")
    public Account findByUsername(@RequestParam String username){
        return accountService.findByUsername(username);
    }

    @PostMapping("/create") // /api/v1/account/create
    public Account create(@RequestBody @Valid AccountCreatDto accountCreatDto){
        return accountService.create(accountCreatDto);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable int id){
        try {
            accountService.delete(id);
            return "Xoá thành công";
        } catch (Exception exception){
            return exception.getMessage();
        }
    }

    @PutMapping("/update")
    public Account update(@RequestBody AccountUpdateDto dto){
        return accountService.update(dto);
    }
}
