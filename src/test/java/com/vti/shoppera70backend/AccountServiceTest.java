package com.vti.shoppera70backend;

import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.repository.AccountRepository;
import com.vti.shoppera70backend.service.AccountService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class) // Định nghĩa class chạy Unit Test
public class AccountServiceTest {
    @Mock // các mock object phụ thuộc
    private AccountRepository accountRepository;

    @InjectMocks // Inject các mock object để viết unit test
    private AccountService accountService;

    @Test
    public void getById_success(){
        // Mock giá trị trả về khi đối tượng mock (accountService) gọi tới hàm accountRepository.findById
        // Có thể truyền vào số cụ thể bất kỳ hoặc để ArgumentMatchers.anyInt()
        Mockito.when(accountRepository.findById(1)).thenReturn(Optional.of(new Account()));
        // Chạy mock method accountService.getById.
        // Trường hợp đang test là có dữ liệu trả về thành công,
        // để kiểm tra kết quả trả về là khác null ta sử dụng phương thức
        // Assert.assertNotNull của framework JUnit
        Assert.assertNotNull(accountService.getById(1));
    }

    @Test(expected = RuntimeException.class)
    public void getById_fails_notFound(){
        // Mock giá trị trả về khi đối tượng mock (accountService) gọi tới hàm accountRepository.findById
        // Có thể truyền vào số cụ thể bất kỳ hoặc để ArgumentMatchers.anyInt()
        Mockito.when(accountRepository.findById(ArgumentMatchers.anyInt())).thenReturn(Optional.empty());
        // Chạy mock method accountService.getById.
        // Trường hợp đang test là ko có dữ liệu trả về và hàm bắn ra 1 ngoại lệ là RuntimeException,
        // để kiểm tra kết quả trả về là 1 ngoại lệ, ta sử dụng @Test(expected = RuntimeException.class)
        // và gọi trực tiếp tới phương thức cần kiểm tra
        accountService.getById(ArgumentMatchers.anyInt());
    }
}