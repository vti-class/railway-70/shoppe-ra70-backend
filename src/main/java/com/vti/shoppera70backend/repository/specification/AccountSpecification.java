package com.vti.shoppera70backend.repository.specification;

import com.vti.shoppera70backend.modal.dto.SearchAccountRequest;
import com.vti.shoppera70backend.modal.entity.Account;
import org.springframework.data.jpa.domain.Specification;

public class AccountSpecification {
    public static Specification<Account> buildCondition(SearchAccountRequest request){
        return Specification.where(findByUsername(request))
                .and(findByRole(request));
    }

    //Specification<Account>: 1 Kiểu dữ liệu về 1 điều kiện của Account
    private static Specification<Account> findByUsername(SearchAccountRequest request) {
        if (request.getUsername() != null) {
            // Tạo điều kiện tìm kiếm với name
            return (root, query, cri) -> {
// root: Chọn cột, field, để tìm kiếm (giá trị là thuộc tính trong java)
// cri: CriteriaBuilder Khai báo loại so sánh dữ liệu. ( lớn hơn, nhỏ hơn, equal, like,.... )
                return cri.like(root.get("username"), "%" + request.getUsername() + "%");
            };
        } else {
            return null; // sẽ trả về tất cả dữ liệu (bỏ qua điều kiện là username)
        }
    }

    private static Specification<Account> findByRole(SearchAccountRequest request) {
        if (request.getRole() != null) {
            // Tạo điều kiện tìm kiếm với name
            return (root, query, cri) -> {
// root: Chọn cột, field, để tìm kiếm (giá trị là thuộc tính trong java)
// cri: CriteriaBuilder Khai báo loại so sánh dữ liệu. ( lớn hơn, nhỏ hơn, equal, like,.... )
                return cri.equal(root.get("role"), request.getRole());
            };
        } else {
            return null; // sẽ trả về tất cả dữ liệu (bỏ qua điều kiện là username)
        }
    }
}
