package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.dto.OrderCreateDto;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Order;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import com.vti.shoppera70backend.modal.entity.Product;
import com.vti.shoppera70backend.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OderService implements IOrderService{
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IProductService productService;

    @Override
    public List<Order> getByOrderStatus(OrderStatus orderStatus, int accountId) {
        if (orderStatus == null){
            return orderRepository.findAllByOrderBy_Id(accountId);
        } else {
            return orderRepository.findAllByOrderStatusAndOrderBy_Id(orderStatus, accountId);
        }
    }

    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order getById(int id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()){
            return orderOptional.get();
        }
        return null;
    }

    @Override
    public void delete(int id) {
        if (orderRepository.existsById(id)){
            orderRepository.deleteById(id);
        } else {
            throw new RuntimeException("Id không tồn tại");
        }
    }

    @Override
    public Order create(OrderCreateDto dto) {
        Account account = accountService.getById(dto.getAccountId());
        Product product = productService.getById(dto.getProductId());
        Order order = new Order();
        order.setOrderBy(account);
        order.setProduct(product);
        order.setQuantity(dto.getQuantity());
        order.setOrderStatus(OrderStatus.PENDING);
        return orderRepository.save(order);
    }
}
