package com.vti.shoppera70backend.schedule;

import com.vti.shoppera70backend.modal.entity.Order;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import com.vti.shoppera70backend.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Component
public class MyJob {

    @Autowired
    private OrderRepository orderRepository;

    @Scheduled(cron = "0 0/1 * * * *")
    public void checkOrder(){
        System.out.println("Job start at: " + Instant.now());
        // Thao tác job
        // Lấy danh sách Order đã quá hạn(ngày tạo: 3 ngày trước, trạng thái: pedding)
        long dateNowLong = new Date().getTime();
        long _3day = 24*60*60*100*3; // 25 920 000
        Date condition = new Date(dateNowLong - _3day);
        List<Order> orders = orderRepository.findAllByCreateAtBeforeAndOrderStatus(condition, OrderStatus.PENDING);
        // Set các phần từ -> status= cancel
        for (Order order: orders) {
            order.setOrderStatus(OrderStatus.CANCEL);
        }
        // Save all
        orderRepository.saveAll(orders);
        System.out.println("Đã update: " + orders.size() + " order");
    }
}
