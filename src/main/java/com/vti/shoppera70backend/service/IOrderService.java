package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.dto.OrderCreateDto;
import com.vti.shoppera70backend.modal.entity.Order;
import com.vti.shoppera70backend.modal.entity.OrderStatus;

import java.util.List;

public interface IOrderService {
    List<Order> getByOrderStatus(OrderStatus orderStatus, int accountId);

    List<Order> getAll();

    Order getById(int id);

    void delete(int id);

    Order create(OrderCreateDto dto);
}
