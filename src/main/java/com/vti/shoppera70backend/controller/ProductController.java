package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.config.exception.CustomException;
import com.vti.shoppera70backend.modal.dto.ProductCreateDto;
import com.vti.shoppera70backend.modal.dto.ProductUpdateDto;
import com.vti.shoppera70backend.modal.dto.SearchAccountRequest;
import com.vti.shoppera70backend.modal.dto.SearchProductRequest;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Product;
import com.vti.shoppera70backend.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

@RestController // return -> json (mô hình REST API)
//@Controller // return -> html (mô hình mvc)
@RequestMapping(value = "/api/v1/product")
@CrossOrigin(value = "*")
@Validated
public class ProductController {
    @Autowired
    private IProductService productService;

    @GetMapping("/get-all")// api: localhost:8080/api/v1/product
    public List<Product> getAll(){
        return productService.getAll();
    }

    @PostMapping("/search")
    public Page<Product> search(@RequestBody SearchProductRequest request){
        return productService.search(request);
    }

    @GetMapping("/{id}")
    public Product getById(@PathVariable int id){
        return productService.getById(id);
    }

    @PostMapping("/create") // /api/v1/product/create
    @PreAuthorize("hasAuthority('ADMIN')")
    public Product create(@RequestBody @Valid ProductCreateDto dto){
        return productService.create(dto);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String delete(@PathVariable int id){
            productService.delete(id);
            return "Xoá thành công";

    }

    @PutMapping("/update")
    public Product update(@RequestBody ProductUpdateDto dto){
        return productService.update(dto);
    }

}
