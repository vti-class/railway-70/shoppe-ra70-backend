package com.vti.shoppera70backend.modal.entity;

public enum ProductType {
    PHONE, COMPUTER, CLOTHES, FOOT_WEAR;
}
