package com.vti.shoppera70backend.validation;

import com.vti.shoppera70backend.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

//GHI CHÚ: ConstraintValidator< Annotation đã tạo, kiểu dữ liệu muốn kiểm tra>
public class UsernameExistsValidator implements ConstraintValidator<UsernameExists , String> {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        return !accountRepository.existsByUsername(username);
    }
}
