package com.vti.shoppera70backend.modal.entity;

import com.vti.shoppera70backend.modal.dto.AccountCreatDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "`Account`")
@Getter
@Setter
@NoArgsConstructor // Tạo ra hàm khởi tạo ko tham số
@AllArgsConstructor // Tạo ra hàm khởi tạo full tham số
public class Account extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "USERNAME", length = 50, unique = true, nullable = false)
    private String username;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE")
    private Role role;

    @Column(name = "PHONE_NUMBER", length = 12, unique = true, nullable = false)
    private String phoneNumber;

    @Column(name = "PASSWORD", length = 50, nullable = false)
    private String password;

    @Column(name = "INFORMATION", length = 500)
    private String information;

    @Column(name = "FULL_NAME", length = 100)
    private String fullName;

    @Column(name = "EMAIL", length = 50, unique = true, nullable = false)
    private String email;

    @Column(name = "DATE_OF_BIRTH")
    private Date dateOfBirth;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "IS_ACTIVE")
    private boolean isActive;// Khi tạo mới 1 tài khoản, giá trị này mặc định là false

    public Account(AccountCreatDto dto) {
        this.address = dto.getAddress();
    }
}
