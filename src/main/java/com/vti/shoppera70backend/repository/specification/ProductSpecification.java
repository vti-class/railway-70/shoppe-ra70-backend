package com.vti.shoppera70backend.repository.specification;

import com.vti.shoppera70backend.modal.dto.SearchAccountRequest;
import com.vti.shoppera70backend.modal.dto.SearchProductRequest;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Product;
import org.springframework.data.jpa.domain.Specification;

public class ProductSpecification {
    public static Specification<Product> buildCondition(SearchProductRequest request){
        return Specification.where(findByProductName(request))
                .and(findByProductTypes(request))
                .and(findByShippingUnits(request))
                .and(findByProductStatuses(request))
                .and(findByPrice(request));
    }

    private static Specification<Product> findByProductTypes(SearchProductRequest request) {
        if (request.getProductTypes() != null && request.getProductTypes().size() > 0) {
            return (root, query, cri) -> {
                // Tạo điều kiện tìm kiếm với productType. Producttype sẽ là 1 trong các giá trị truyền vào
                return root.get("productType").in(request.getProductTypes());
            };
        } else {
            return null;
        }
    }

    private static Specification<Product> findByShippingUnits(SearchProductRequest request) {
        if (request.getShippingUnits() != null && request.getShippingUnits().size() > 0) {
            return (root, query, cri) -> {
                // Tạo điều kiện tìm kiếm với productType. Producttype sẽ là 1 trong các giá trị truyền vào
                return root.get("shippingUnit").in(request.getShippingUnits());
            };
        } else {
            return null;
        }
    }

    private static Specification<Product> findByProductStatuses(SearchProductRequest request) {
        if (request.getProductStatuses() != null && request.getProductStatuses().size() > 0) {
            return (root, query, cri) -> {
                // Tạo điều kiện tìm kiếm với productType. Producttype sẽ là 1 trong các giá trị truyền vào
                return root.get("productStatus").in(request.getProductStatuses());
            };
        } else {
            return null;
        }
    }

    private static Specification<Product> findByProductName(SearchProductRequest request) {
        if (request.getProductName() != null) {
            // Tạo điều kiện tìm kiếm với name
            return (root, query, cri) -> {
// root: Chọn cột, field, để tìm kiếm (giá trị là thuộc tính trong java)
// cri: CriteriaBuilder Khai báo loại so sánh dữ liệu. ( lớn hơn, nhỏ hơn, equal, like,.... )
                return cri.like(root.get("name"), "%" + request.getProductName() + "%");
            };
        } else {
            return null; // sẽ trả về tất cả dữ liệu (bỏ qua điều kiện là username)
        }
    }

    private static Specification<Product> findByPrice(SearchProductRequest request) {
        if (request.getMaxPrice() != 0) {
            return (root, query, cri) -> {
                return cri.between(root.get("price"), request.getMinPrice(), request.getMaxPrice());
            };
        } else {
            return null; // sẽ trả về tất cả dữ liệu (bỏ qua điều kiện là username)
        }
    }
}
