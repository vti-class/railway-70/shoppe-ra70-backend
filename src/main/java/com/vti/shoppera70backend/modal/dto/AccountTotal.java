package com.vti.shoppera70backend.modal.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.sql.ResultSet;
import java.sql.SQLException;

@Data
@NoArgsConstructor
public class AccountTotal {
    private int id;
    private String username;
    private String productName;
    private int totalQuantity;

    public AccountTotal(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.username = resultSet.getString("username");
        this.productName = resultSet.getString("product_name");
        this.totalQuantity = resultSet.getInt("total_quantity");
    }
}
