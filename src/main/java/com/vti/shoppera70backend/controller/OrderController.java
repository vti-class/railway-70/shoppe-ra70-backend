package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.modal.dto.OrderCreateDto;
import com.vti.shoppera70backend.modal.entity.Order;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import com.vti.shoppera70backend.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController // return -> json (mô hình REST API)
//@Controller // return -> html (mô hình mvc)
@RequestMapping(value = "/api/v1/order")
public class OrderController {
    @Autowired
    private IOrderService orderService;

    @GetMapping("/get-all")// api: localhost:8080/api/v1/order
    public List<Order> getAll(){
        return orderService.getAll();
    }

    @GetMapping("/get-by-orderStatus")
    public List<Order> getByOrderStatus(@RequestParam int accountId, @RequestParam(required = false) OrderStatus orderStatus){
        return orderService.getByOrderStatus(orderStatus, accountId);
    }

    @GetMapping("/{id}")
    public Order getById(@PathVariable int id){
        return orderService.getById(id);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable int id){
            orderService.delete(id);
            return "Xoá thành công";
    }

    @PostMapping("/create") // /api/v1/order/create
    public Order create(@RequestBody OrderCreateDto dto){
        return orderService.create(dto);
    }

}
