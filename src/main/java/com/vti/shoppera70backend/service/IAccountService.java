package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.dto.AccountCreatDto;
import com.vti.shoppera70backend.modal.dto.AccountUpdateDto;
import com.vti.shoppera70backend.modal.dto.SearchAccountRequest;
import com.vti.shoppera70backend.modal.entity.Account;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IAccountService {
    Page<Account> search(SearchAccountRequest request);

    List<Account> getAll();

    Account getById(int id);

    Account findByUsername(String username);

    Account create(AccountCreatDto dto);

    void delete(int id);

    Account update(AccountUpdateDto dto);
}
