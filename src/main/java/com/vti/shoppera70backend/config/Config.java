package com.vti.shoppera70backend.config;

import com.vti.shoppera70backend.modal.dto.DemoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class Config {

    @Bean
    public DemoBean demoBean1(){
        DemoBean demoBean = new DemoBean();
        demoBean.setId(1);
        demoBean.setName("Demo bean 1");
        return demoBean;
    }

    @Bean
    public DemoBean demoBean2(){
        DemoBean demoBean = new DemoBean();
        demoBean.setId(2);
        demoBean.setName("Demo bean 2");
        return demoBean;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
