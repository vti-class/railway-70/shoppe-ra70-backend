package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.validation.UsernameExists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.UniqueElements;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountCreatDto {
    @UsernameExists(message = "{account.create.username_existed}")
    private String username; // username

    private String phoneNumber;
    private String password;
    private String information;
    private String fullName;
    @NotBlank
    private String email;
    private Date dateOfBirth;
    private String address;

}
