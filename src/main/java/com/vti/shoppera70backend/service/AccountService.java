package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.config.exception.CustomException;
import com.vti.shoppera70backend.config.exception.ErrorResponseEnum;
import com.vti.shoppera70backend.modal.dto.AccountCreatDto;
import com.vti.shoppera70backend.modal.dto.AccountUpdateDto;
import com.vti.shoppera70backend.modal.dto.BaseRequest;
import com.vti.shoppera70backend.modal.dto.SearchAccountRequest;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Role;
import com.vti.shoppera70backend.repository.AccountRepository;
import com.vti.shoppera70backend.repository.specification.AccountSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AccountService implements IAccountService, UserDetailsService {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Account getById(int id) {
        Optional<Account> accountOptional = accountRepository.findById(id);
        if (accountOptional.isEmpty()){
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_ACCOUNT);
        }
        return accountOptional.get();
    }


    @Override
    public Page<Account> search(SearchAccountRequest request) {
        BaseRequest.verify(request);
        // Tạo điều kiện tìm kiếm
        Specification<Account> condition = AccountSpecification.buildCondition(request);
        // Tạo điều kiện phân trang, sắp xếp.
        PageRequest pageRequest = BaseRequest.buildPageRequest(request);

        // Trang đầu tiên của viện là trang 0
        // Trang đầu tiên người dùng muốn định nghĩa là trang 1
        return accountRepository.findAll(condition, pageRequest);
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account findByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    @Override
    public Account create(AccountCreatDto dto) {
        if(accountRepository.existsByUsername(dto.getUsername())){
            throw new CustomException(ErrorResponseEnum.USERNAME_EXISTED);
        }
        // Chuyển đổi từ dto -> entity
        // C1: Dùng get set
        Account account = new Account();

        // C2: Dùng thư viện (modal mapper, BeanUtils)
        // BeanUtils.copyProperties(obj1, obj2);
        // obj1: Đối tượng để láy giá trị, obj2: gán giá trị từ thuộc tính của obj1
        BeanUtils.copyProperties(dto, account);
        account.setRole(Role.CUSTOMER);
        account.setPassword(passwordEncoder.encode(dto.getPassword())); // Set password đã mã hoá vào DB
        return accountRepository.save(account); // hàm repo.save() cần truyền vào giá trị của Account muốn thêm mới;
    }

    @Override
    public void delete(int id) {
        if (accountRepository.existsById(id)){
            accountRepository.deleteById(id);
        } else {
//            throw new Exception("Id không tồn tại");
            throw new RuntimeException("Id không tồn tại");// Tạo ra 1 class để xử lý exception
        }

    }

    @Override
    public Account update(AccountUpdateDto dto) {
        Account account = getById(dto.getId()); // Giá trị account khi chưa update
//        account.setUsername(dto.getUsername());
//        account.setPhoneNumber(dto.getPhoneNumber());
        BeanUtils.copyProperties(dto, account);
        return accountRepository.save(account);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUsername(username);
        if (account == null){
            throw new UsernameNotFoundException("Username không tồn tại!");
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(account.getRole());
        return new User(account.getUsername(), account.getPassword(), authorities);
    }
}
