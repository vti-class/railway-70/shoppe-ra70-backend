package com.vti.shoppera70backend.modal.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass // Để đánh dấu class này cũng là 1 phần trong các entity khác
public class EntityBase {
    @Column(name = "CREATE_BY")
    protected String createBy;

    @Column(name = "CREATE_AT")
    protected Date createAt;

    @Column(name = "UPDATE_BY")
    protected String updateBy;

    @Column(name = "UPDATE_AT")
    protected Date updateAt;

    // Hàm này gọi tới khi đối tượng entity được thêm mới.
    @PrePersist
    public void prePersist(){
        this.createAt = new Date();
        try {
            this.createBy = SecurityContextHolder.getContext().getAuthentication().getName();
        } catch (Exception ex){
            this.createBy = "User chưa đăng nhập";
        }
    }

    // Hàm này gọi tới khi đối tượng entity được update.
    @PreUpdate
    public void preUpdate(){
        try {
            this.updateBy = SecurityContextHolder.getContext().getAuthentication().getName();
        } catch (Exception ex){
            this.updateBy = "User chưa đăng nhập";
        }
        this.updateAt = new Date();
    }
}
