package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.Role;
import lombok.Data;

@Data
public class SearchAccountRequest extends BaseRequest{
    private String username;
    private Role role;
    private String phoneNumber;
    private String fullName;
    private String email;
    private String address;
}
