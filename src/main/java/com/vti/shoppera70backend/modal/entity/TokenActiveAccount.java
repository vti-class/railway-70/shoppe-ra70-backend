package com.vti.shoppera70backend.modal.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "`TOKEN`")
@Getter
@Setter
@NoArgsConstructor // Tạo ra hàm khởi tạo ko tham số
@AllArgsConstructor // Tạo ra hàm khởi tạo full tham số
public class TokenActiveAccount extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @OneToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;// FK tới bảng muốn active

    @Column(name = "TOKEN")
    private String token; // Đây là mã dùng để active tài khoản. VD:
}
