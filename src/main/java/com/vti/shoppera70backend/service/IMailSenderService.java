package com.vti.shoppera70backend.service;

public interface IMailSenderService {
    void sendMessageWithAttachment(String to, String subject, String text);
}
