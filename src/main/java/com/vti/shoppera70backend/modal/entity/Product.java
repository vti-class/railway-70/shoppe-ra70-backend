package com.vti.shoppera70backend.modal.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "`Product`")
@Getter
@Setter
@NoArgsConstructor // Tạo ra hàm khởi tạo ko tham số
@AllArgsConstructor // Tạo ra hàm khởi tạo full tham số
public class Product extends EntityBase{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME", length = 255, unique = true, nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private ProductStatus productStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "SHIPPING_UNIT")
    private ShippingUnit shippingUnit;

    @Enumerated(EnumType.STRING)
    @Column(name = "PRODUCT_TYPE")
    private ProductType productType;

    @Column(name = "PRICE")
    private long price;


    // C1: server(firebase, S3 amazone, gg diver, minIO,...)
    // -> lưu file -> lấy url -> lưu DB

    //C3: Lưu ở máy local: Lưu đường dẫn tuyệt đối hoặc tương đối để lấy file

    //C3: Lưu trực tiếp file vào DB: Hay dùng với project lưu ít file(base64, byte[])
    @Column(name = "IMAGE", length = 500, nullable = false)
    private String image;
}
