package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.config.exception.CustomException;
import com.vti.shoppera70backend.config.exception.ErrorResponseEnum;
import com.vti.shoppera70backend.modal.dto.BaseRequest;
import com.vti.shoppera70backend.modal.dto.ProductCreateDto;
import com.vti.shoppera70backend.modal.dto.ProductUpdateDto;
import com.vti.shoppera70backend.modal.dto.SearchProductRequest;
import com.vti.shoppera70backend.modal.entity.Product;
import com.vti.shoppera70backend.repository.ProductRepository;
import com.vti.shoppera70backend.repository.specification.ProductSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackOn = Exception.class)
public class ProductService implements IProductService{
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Page<Product> search(SearchProductRequest request) {
        BaseRequest.verify(request);

        Specification<Product> condition = ProductSpecification.buildCondition(request);

        PageRequest pageRequest = BaseRequest.buildPageRequest(request);

        return productRepository.findAll(condition, pageRequest);
    }

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public Product getById(int id) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isEmpty()){
           //throw new RuntimeException("Id ko tồn tại"); // CustomException: có các giá trị tg, mess, status
            throw new CustomException(ErrorResponseEnum.NOT_FOUND_PRODUCT);
        }
        return productOptional.get();
    }

    @Override
    public Product create(ProductCreateDto dto) {
        Product product = new Product();
        BeanUtils.copyProperties(dto, product);
        return productRepository.save(product);
    }

    @Override
    public Product update(ProductUpdateDto dto) {
        Product product = getById(dto.getId());
        BeanUtils.copyProperties(dto, product);
        return productRepository.save(product);
    }

    @Override
    public void delete(int id) {
        Product product = getById(id);
        productRepository.delete(product);
    }
}
